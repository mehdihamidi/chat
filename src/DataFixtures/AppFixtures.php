<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class AppFixtures
 * @package App\DataFixtures
 */
class AppFixtures extends Fixture
{
    /** @var UserPasswordEncoderInterface  */
    private $encoder;

    /**
     * AppFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     */
    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setFirstname('Mehdi');
        $user1->setLastname('HAMIDI');
        $user1->setEmail('mehdi@gmail.com');
        $user1->setPassword($this->encoder->encodePassword($user1, 'azerty'));
        $manager->persist($user1);

        $user2 = new User();
        $user2->setFirstname('Sarah');
        $user2->setLastname('HAMIDI');
        $user2->setEmail('sarah@gmail.com');
        $user2->setPassword($this->encoder->encodePassword($user2, 'azerty'));
        $manager->persist($user2);

        $user3 = new User();
        $user3->setFirstname('Rime');
        $user3->setLastname('HAMIDI');
        $user3->setEmail('rime@gmail.com');
        $user3->setPassword($this->encoder->encodePassword($user3, 'azerty'));
        $manager->persist($user3);

        $user4 = new User();
        $user4->setFirstname('Nouredine');
        $user4->setLastname('HAMIDI');
        $user4->setEmail('nouredine@gmail.com');
        $user4->setPassword($this->encoder->encodePassword($user4, 'azerty'));
        $manager->persist($user4);

        $manager->flush();
    }
}
