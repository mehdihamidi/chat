<?php

namespace App\Dto;

use App\Entity\User;

/**
 * Class Register
 * @package App\Dto
 */
class Register
{
    /** @var string */
    private $firstname;

    /** @var string */
    private $lastname;

    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        $user = new User();
        $user->setFirstname($this->firstname);
        $user->setLastname($this->lastname);
        $user->setEmail($this->email);
        $user->setPassword($this->password);

        return $user;
    }
}