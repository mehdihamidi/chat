'use strict';

import './libs/jquery.min.js';
import './libs/bootstrap.bundle.min.js';
import './libs/metisMenu.min.js';
import './libs/simplebar.min.js';
import './libs/waves.min.js';
import './app.js';

